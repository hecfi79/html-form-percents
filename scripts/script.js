function calculateInterest() {
    const principalInput = document.getElementById("principal"); // Получаем значения полей формы
    const interestRateInput = document.getElementById("interest-rate");
    const durationInput = document.getElementById("duration");
    const compoundingPeriodInput = document.getElementById("compounding-period");
    const interestTypeInput = document.getElementById("interest-type");
    const principal = parseFloat(principalInput.value); // Переводим в нужные типы данных
    const interestRate = parseFloat(interestRateInput.value);
    const duration = parseFloat(durationInput.value);
    const compoundingPeriod = parseFloat(compoundingPeriodInput.value);
    const interestType = interestTypeInput.value;
    let interest; // Вычисляем проценты
    if (interestType === "simple") {
        interest = principal * interestRate / 100 * duration / 365;
    } else {
        const periods = duration / compoundingPeriod;
        const ratePerPeriod = interestRate / 100 / periods;
        interest = principal * (Math.pow(1 + ratePerPeriod, periods) - 1);
    }
    const resultDiv = document.getElementById("result"); // Выводим результаты
    resultDiv.textContent = "Проценты: " + interest.toFixed(2);
}